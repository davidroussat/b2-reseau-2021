# TP3 : Progressons vers le réseau d'infrastructure

Ce qu'on entend par "réseau" d'infrastructure c'est d'**adopter un point de vue admin** plutôt qu'un point de vue client.

Nous allons nous **positionner** de plus en plus comme les **personnes qui entretiennent le réseau** (admin), et non simplement comme les **personnes qui consomment le réseau** (client).

De quoi est composé, sommairement un réseau digne de ce nom :

- **une architecture réseau**
  - c'est l'organisation des switches, routeurs, firewall, etc, ainsi que le câblage entre eux
  - redondance des équipements, des logiciels, des solutions
  - on en verra que peu pour le moment
- **des services réseau d'infra**
  - des services très récurrents, nécessaire au fonctionnement normal d'un parc info
  - on parle ici, entre autres, de : DNS, DHCP, etc.
- **de services réseau orientés sur le maintien en condition opérationnelle**
  - accès aux machines via SSH
  - monitoring
  - sauvegarde
- **d'autres services, utiles directement aux clients du réseau**
  - serveur web
  - serveur de partage de fichiers

**BAH.** On est partis :D

![Here we gooo](./pic/sysadmin-hotline.gif "Here we gooo")

# Sommaire

- [TP3 : Progressons vers le réseau d'infrastructure](#tp3--progressons-vers-le-réseau-dinfrastructure)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. (mini)Architecture réseau](#i-miniarchitecture-réseau)
  - [1. Adressage](#1-adressage)
  - [2. Routeur](#2-routeur)
- [II. Services d'infra](#ii-services-dinfra)
  - [1. Serveur DHCP](#1-serveur-dhcp)
  - [2. Serveur DNS](#2-serveur-dns)
    - [A. Our own DNS server](#a-our-own-dns-server)
    - [B. SETUP copain](#b-setup-copain)
  - [3. Get deeper](#3-get-deeper)
    - [A. DNS forwarder](#a-dns-forwarder)
    - [B. On revient sur la conf du DHCP](#b-on-revient-sur-la-conf-du-dhcp)
- [Entracte](#entracte)
- [III. Services métier](#iii-services-métier)
  - [1. Serveur Web](#1-serveur-web)
  - [2. Partage de fichiers](#2-partage-de-fichiers)
    - [A. L'introduction wola](#a-lintroduction-wola)
    - [B. Le setup wola](#b-le-setup-wola)
- [IV. Un peu de théorie : TCP et UDP](#iv-un-peu-de-théorie--tcp-et-udp)
- [V. El final](#v-el-final)

# 0. Prérequis

➜ On reste sur full Rocky Linux côté machines virtuelles pour le tp : les clients, les routeurs, les serveurs. All Rocky meow. **N'hésitez pas à descendre à 1024Mo de RAM, voire moins. On a pas besoin de grand chose, et il va y avoir plusieurs machines dans ce TP.**

➜ [Référez-vous au mémo dédié pour les paquets à pré-installer dans le patron, ainsi que la conf à y effectuer.](../../cours/memo/install_vm.md)

➜ **Aucune carte NAT, à part sur la machine `router`** qui donnera un accès internet à tout le monde.

➜ On distinguera trois types de noeuds pendant le TP :

- *routeurs* :
  - IP statiques
  - routes statiques si besoin
- *serveurs* :
  - IP statiques
  - routes statiques si besoin
- *clients* :
  - IP dynamiques (récupérées en DHCP)
  - routes dynamiques (récupérées en DHCP)

➜ **Vous ne créerez aucune machine au début. Vous les créerez au fur et à mesure que le TP vous le demande.** A chaque fois qu'une nouvelle machine devra être créée, vous trouverez l'emoji 🖥️ avec son nom.

## Checklist

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel
- [x] accès Internet (une route par défaut donc :) )
  - le routeur a une carte NAT
  - les autres machines passent par le routeur pour avoir internet (ajout de route par défaut)
- [x] accès à tous les LANs
  - route statiques ou dynamiques, suivant le type de noeud (voir un peu plus bas)
- [x] résolution de nom
  - vers internet
  - en local, grâce à votre propre serveur DNS (une fois qu'il sera monté, au début, ignorez cette étape)
- [x] modification des fichiers de zone DNS
  - attendez d'avoir passer l'étape du DNS pour faire ça à chaque fois
  - au début, ignorez cette étape

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. (mini)Architecture réseau

**Le but ici va être de mettre en place 3 réseaux, dont vous choisirez vous-mêmes les adresses.** Les contraintes :

- chaque réseau doit commencer par `10.3.`
  - `10` -> adresse privée
  - `3` -> c'est notre troisième TP :)
- chaque réseau doit être **le plus petit possible** : choisissez judicieusement vos masques
- aucun réseau ne doit se superposer (pas de doublons d'adresses IP possible)
- il y aura
  - deux réseaux pour des serveurs
  - un réseau pour des clients
- en terme de taille de réseau, vous compterez
  - 35 clients max pour le réseau `client1`
  - 63 serveurs max pour le réseau `server1`
  - 10 serveurs max pour le réseau `server2`
- les réseaux doivent, autant que possible, se suivre
- [référez-vous au cours dédié au subnetting](../../cours/ip/README.md#2-subnetting) si vous ne savez pas comment vous y prendre

> *Comme d'hab, pour le moment, chaque réseau = un host-only dans VirtualBox. Vous attribuerez comme IP à votre carte host-only (sur l'hôte, votre PC) **la première IP disponible du réseau.***

---

**Il y aura un routeur dans le TP**, qui acheminera les paquets d'un réseau à l'autre. Ce routeur sera donc la **passerelle** des 3 réseaux.  
On peut aussi dire qu'il sera la passerelle des machines, dans chacun des réseaux.  

> *Cette machine aura donc une carte réseau dans TOUS les réseaux.*

**Pour ce routeur, vous respecterez la convention qui consiste à lui attribuer la dernière IP disponible du réseau dans lequel il se trouve.**

> *Par exemple, dans le réseau `192.168.10.0/24`, par convention, on définira sur le routeur l'adresse IP `192.168.10.254`.*

## 1. Adressage

🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.?.?`        | `255.255.?.?` | ?                           | `10.3.?.?`         | `10.3.?.?`                                                                                   |
| `server1`     | `10.3.?.?`        | `255.255.?.?` | ?                           | `10.3.?.?`         | `10.3.?.?`                                                                                   |
| `server2`     | `10.3.?.?`        | `255.255.?.?` | ?                           | `10.3.?.?`         | `10.3.?.?`                                                                                   |

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.?.?/?`         | `10.3.?.?/?`         | `10.3.?.?/?`         | Carte NAT             |
| ...          | ...                  | ...                  | ...                  | `10.3.?.?/?`          |

> *N'oubliez pas de **TOUJOURS** fournir le masque quand vous écrivez une IP.*

## 2. Routeur

🖥️ **VM router.tp3**

🌞 **Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**

- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle
- il a un accès internet
- il a de la résolution de noms
- il porte le nom `router.tp3`*
- n'oubliez pas [d'activer le routage sur la machine](../../cours/memo/rocky_network.md#activation-du-routage)

# II. Services d'infra

Ce qu'on appelle un "service d'infra" c'est un service qui n'est pas directement utile pour un utilisateur du réseau. Ce n'est pas un truc que l'utilisateur va consommer sciemment, pour obtenir quelque chose.  

Un service d'infra, c'est un service nécessaire pour que l'infra tienne debout, dans des conditions normales de fonctionnement. On trouve, entre autres :

- serveurs DHCP
- serveurs DNS
- serveurs de sauvegarde
- serveurs d'annuaires (Active Directory par exemple)
- serveurs de monitoring (surveillance)

## 1. Serveur DHCP

You already did this in TP 2. Repeat.

🖥️ **VM `dhcp.client1.tp3`**

🌞 **Mettre en place une machine qui fera office de serveur DHCP** dans le réseau `client1`. Elle devra :

- porter le nom `dhcp.client1.tp3`
- [📝**checklist**📝](#checklist)
- donner une IP aux machines clients qui le demande
- leur donner l'adresse de leur passerelle
- leur donner l'adresse d'un DNS utilisable

📁 **Fichier `dhcpd.conf`**

---

🖥️ **VM marcel.client1.tp3**

🌞 **Mettre en place un client dans le réseau `client1`**

- de son p'tit nom `marcel.client1.tp3`
- [📝**checklist**📝](#checklist)
- la machine récupérera une IP dynamiquement grâce au serveur DHCP
- ainsi que sa passerelle et une adresse d'un DNS utilisable

🌞 **Depuis `marcel.client1.tp3`**

- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP
- à l'aide de la commande `traceroute`, prouver que `marcel.client1.tp3` passe par `router.tp3` pour sortir de son réseau

## 2. Serveur DNS

### A. Our own DNS server

Le principe, c'est qu'une fois ce DNS en place, vous n'aurez plus besoin des fichiers `/etc/hosts` sur toutes les machines.  
**Le DNS agit comme un répertoire central, il connaît les noms et IP de tout le monde.**  
Une fois qu'il est là, toutes les machines peuvent lui demander à quelle IP se trouve tel ou tel nom.

On dira dans un premier temps que votre serveur DNS est un serveur DNS ***resolver*** pour votre domaine. C'est à dire qu'il va activement résoudre des noms vers des IP (forward), si on lui pose des questions.  
**Il sera *resolver* pour les zones `server1.tp3` et `server2.tp3`.**

**On dit aussi que votre serveur est ***autoritatif*** pour les zones `server1.tp3` et `server2.tp3` ** car c'est lui qui a autorité sur ces zone. C'est lui le serveur qui **SAIT** à quelle IP correspond quel nom.

Dans un deuxième temps, vous lui donnerez aussi, en plus de la fonctionnalité de *resolver* la fonctionnalité de ***forwarder***.  
Il sera alors capable, s'il ne connaît pas la réponse à une requête DNS, de la faire passer à un autre serveur DNS.  

L'idée du *forwarder* ici c'est clairement de lui passer `8.8.8.8` ou `1.1.1.1` en adresse vers qui forward. Comme ça, il répond aux questions pour nos zones en `.tp3` et pour tout le reste, il demande à des DNS publics d'internet.  
**Conséquence** : nos clients pourront résoudre des noms locaux comme `marcel.client1.tp3` mais aussi des noms publics comme `google.com`.

A la fin donc, votre serveur sera :

- autoritatif pour les zones `server1.tp3` et `server2.tp3`
- resolver local
- forwarder (pour résoudre les noms publics)

**C BO NON ?!** 🔥🔥🔥🔥

### B. SETUP copain

🖥️ **VM dns1.server1.tp3**

> Nous avons dans ce TP 2 zones à gérer : `server1.tp3`, `server2.tp3`. J'avoue j'suis pas gentil, ça fait beaucoup de fichiers direct. Ca fait manipuler :D

🌞 **Mettre en place une machine qui fera office de serveur DNS**

- dans le réseau `server1`
- de son p'tit nom `dns1.server1.tp3`
- [📝**checklist**📝](#checklist)
- il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme `google.com`
  - conf classique avec le fichier `/etc/resolv.conf` ou les fichiers de conf d'interface
- comme pour le DHCP, on part sur "rocky linux dns server" on Google pour les détails de l'install du serveur DNS
  - le paquet que vous allez installer devrait s'appeler **`bind` : c'est le nom du serveur DNS le plus utilisé au monde**
- il y aura plusieurs fichiers de conf :
  - un fichier de conf principal `named.conf`
  - des fichiers de zone "forward"
    - permet d'indiquer une correspondance nom -> IP
    - un fichier par zone forward
  - **vous ne mettrez pas en place de zones reverse, uniquement les forward**
  - on ne met **PAS** les clients dans les fichiers de zone car leurs adresses IP peuvent changer (ils les récupèrent à l'aide du DHCP)
    - donc votre DHCP gérera deux zones : `server1.tp3` et `server2.tp3`
    - les réseaux où les IPs sont définies de façon statique !

> Vous avez donc 3 fichiers à gérer : le fichier de conf principal `named.conf`. Un fichier de zone forward pour la zone `server1.tp3` : `server1.tp3.forward`. Et un  fichier similaire pour la zone `server2.tp3`.

> 💡 Je vous recommande de d'abord mettre en place une simple zone *Forward* et de tester. Et après, d'ajouter la deuxième zone. Procédez de façon itérative, pour comprendre d'où viennent vos erreurs quand elles surviennent.

🌞 **Tester le DNS depuis `marcel.client1.tp3`**

- définissez **manuellement** l'utilisation de votre serveur DNS
- essayez une résolution de nom avec `dig`
  - une résolution de nom classique
    - `dig <NOM>` pour obtenir l'IP associée à un nom
    - on teste la zone forward
- prouvez que c'est bien votre serveur DNS qui répond pour chaque `dig`

Par exemple, vous devriez pouvoir exécuter les commandes suivantes :

```bash
$ dig marcel.client1.tp3
$ dig dhcp.client1.tp3
```

> Pour rappel, avec `dig`on peut préciser directement sur la ligne de commande à quel serveur poser la question avec  le caractère `@`. Par exemple `dig google.com @8.8.8.8` permet de préciser explicitement qu'on veut demander à `8.8.8.8` à quelle IP se trouve `google.com`.

⚠️ **NOTE : A partir de maintenant, vous devrez modifier les fichiers de zone pour chaque nouvelle machine ajoutée (voir [📝**checklist**📝](#checklist).**

🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

- les serveurs, on le fait à la main
- les clients, c'est fait *via* DHCP

⚠️**A partir de maintenant, vous n'utiliserez plus DU TOUT les IPs pour communiquer entre vos machines, mais uniquement leurs noms.**

## 3. Get deeper

On va affiner un peu la configuration des outils mis en place.

### A. DNS forwarder

🌞 **Affiner la configuration du DNS**

- faites en sorte que votre DNS soit désormais aussi un forwarder DNS
- c'est à dire que s'il ne connaît pas un nom, il ira poser la question à quelqu'un d'autre

> Hint : c'est la clause `recursion` dans le fichier `/etc/named.conf`. Et c'est déjà activé par défaut en fait.

🌞 Test !

- vérifier depuis `marcel.client1.tp3` que vous pouvez résoudre des noms publics comme `google.com` en utilisant votre propre serveur DNS (commande `dig`)
- pour que ça fonctionne, il faut que `dns1.server1.tp3` soit lui-même capable de résoudre des noms, avec `1.1.1.1` par exemple

### B. On revient sur la conf du DHCP

🖥️ **VM johnny.client1.tp3**

🌞 **Affiner la configuration du DHCP**

- faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients
- créer un nouveau client `johnny.client1.tp3` qui récupère son IP, et toutes les nouvelles infos, en DHCP

---

# Entracte

**YO !**

**On se pose deux minutes pour apprécier le travail réalisé.**

A ce stade vous avez :

- un routeur qui permet aux machines d'acheminer leur trafic entre les réseaux
  - entre les LANs
  - vers internet
- un DHCP qui filent des infos à vos clients
  - une IP, une route par défaut, l'adresse d'un DNS
- un DNS
  - qui résout tous les noms localement

Vous le sentez là que ça commence à prendre forme oupa ? Tous les réseaux du monde sont fichus comme ça c:

**Allez, prend un cookie tu l'as mérité : 🍪**

![You get a cookie](./pic/you-get-a-cookie-ricky-berwick.gif "You get a cookie")

---

# III. Services métier

Ce qu'on appelle un "service métier", à l'inverse du service d'infra, c'est un truc que l'utilisateur final veut consommer. On le dit "métier" car dans une entreprise, c'est ce service qui sert le métier de l'entreprise, son coeur d'activité.

Par exemple, pour une agence de développement web, l'un des services métier, bah c'est les serveurs web qui sont déployés pour porter les sites web développés par la boîte.

> On est en 2021, le serveur web c'est le cas d'école. Y'en a partout. Mais genre + que vous imaginez, même nous les admins, on utilise de plus en plus d'interfaces web, au détriment de la ligne de commande.

## 1. Serveur Web

🖥️ **VM web1.server2.tp3**

🌞 **Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

- réseau `server2`
- hello `web1.server2.tp3` !
- [📝**checklist**📝](#checklist)
- vous utiliserez le serveur web que vous voudrez, le but c'est d'avoir un serveur web fast, pas d'y passer 1000 ans :)
  - réutilisez [votre serveur Web du TP1 Linux](https://gitlab.com/it4lik/b2-linux-2021/-/tree/main/tp/1#2-cr%C3%A9ation-de-service)
  - ou montez un bête NGINX avec la page d'accueil (ça se limite à un `dnf install` puis `systemctl start nginx`)
  - ou ce que vous voulez, du moment que c'est fast
  - **dans tous les cas, n'oubliez pas d'ouvrir le port associé dans le firewall** pour que le serveur web soit joignable

> Une bête page HTML fera l'affaire. On est pas là pour faire du design. Et vous prenez pas la tête pour l'install, appelez-moi vite s'il faut, on est pas en système ni en Linux non plus. On est en réseau, on veut juste un serveur web derrière un port :)

🌞 **Test test test et re-test**

- testez que votre serveur web est accessible depuis `marcel.client1.tp3`
  - utilisez la commande `curl` pour effectuer des requêtes HTTP

---

👋👋👋 **HEY ! C'est beau là.** On a un client qui consomme un serveur web, avec toutes les infos récupérées en DHCP, DNS, blablabla + un routeur maison.  

Je sais que ça se voit pas trop avec les `curl`. Vous pourriez installer un Ubuntu graphique sur le client si vous voulez vous y croire à fond, avec un ptit Firefox, Google Chrome ou whatever.  

**Mais là on y est**, vous avez un ptit réseau, un vrai, avec tout ce qu'il faut.

## 2. Partage de fichiers

### A. L'introduction wola

Dans cette partie, on va monter un serveur NFS. C'est un serveur qui servira à partager des fichiers à travers le réseau.  

**En d'autres termes, certaines machines pourront accéder à un dossier, à travers le réseau.**

Dans notre cas, on va faire en sorte que notre serveur web `web1.server2.tp3` accède à un partage de fichier.

> Dans un cas réel, ce partage de fichiers peut héberger le site web servi par notre serveur Web par exemple.  
Ou, de manière plus récurrente, notre serveur Web peut effectuer des sauvegardes dans ce dossier. Ainsi, le serveur de partage de fichiers devient le serveur qui centralise les sauvegardes.

### B. Le setup wola

🖥️ **VM nfs1.server2.tp3**

🌞 **Setup d'une nouvelle machine, qui sera un serveur NFS**

- réseau `server2`
- son nooooom : `nfs1.server2.tp3` !
- [📝**checklist**📝](#checklist)
- je crois que vous commencez à connaître la chanson... Google "nfs server rocky linux"
  - [ce lien me semble être particulièrement simple et concis](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=1)
- **vous partagerez un dossier créé à cet effet : `/srv/nfs_share/`**

🌞 **Configuration du client NFS**

- effectuez de la configuration sur `web1.server2.tp3` pour qu'il accède au partage NFS
- le partage NFS devra être monté dans `/srv/nfs/`
- [sur le même site, y'a ça](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=2)

🌞 **TEEEEST**

- tester que vous pouvez lire et écrire dans le dossier `/srv/nfs` depuis `web1.server2.tp3`
- vous devriez voir les modifications du côté de  `nfs1.server2.tp3` dans le dossier `/srv/nfs_share/`

# IV. Un peu de théorie : TCP et UDP

Bon bah avec tous ces services, on a de la matière pour bosser sur TCP et UDP. P'tite partie technique pure avant de conclure.

🌞 **Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :**

- SSH
- HTTP
- DNS
- NFS

📁 **Captures réseau `tp3_ssh.pcap`, `tp3_http.pcap`, `tp3_dns.pcap` et `tp3_nfs.pcap`**

> **Prenez le temps** de réfléchir à pourquoi on a utilisé TCP ou UDP pour transmettre tel ou tel protocole. Réfléchissez à quoi servent chacun de ces protocoles, et de ce qu'on a besoin qu'ils réalisent.

🌞 **Expliquez-moi pourquoi je ne pose pas la question pour DHCP.**

🌞 **Capturez et mettez en évidence un *3-way handshake***

📁 **Capture réseau `tp3_3way.pcap`**

# V. El final

🌞 **Bah j'veux un schéma.**

- réalisé avec l'outil de votre choix
- un schéma clair qui représente
  - les réseaux
    - les adresses de réseau devront être visibles
  - toutes les machines, avec leurs noms
  - devront figurer les IPs de toutes les interfaces réseau du schéma
  - pour les serveurs : une indication de quel port est ouvert
- vous représenterez les host-only comme des switches
- dans le rendu, mettez moi ici à la fin :
  - le schéma
  - le 🗃️ tableau des réseaux 🗃️
  - le 🗃️ tableau d'adressage 🗃️
    - on appelle ça aussi un "plan d'adressage IP" :)

> J'vous le dis direct, un schéma moche avec Paint c'est -5 Points. Je vous recommande [draw.io](http://draw.io).

🌞 **Et j'veux des fichiers aussi, tous les fichiers de conf du DNS**

- 📁 Fichiers de zone
- 📁 Fichier de conf principal DNS `named.conf`
- faites ça à peu près propre dans le rendu, que j'ai plus qu'à cliquer pour arriver sur le fichier ce serait top
