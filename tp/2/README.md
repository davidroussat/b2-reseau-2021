# TP2 : On va router des trucs

Au menu de ce TP, on va explorer les notions vu en cours un peu plus en profondeur, et vous allez les explorer par vous-mêmes :

- les échanges ARP et DHCP
- le routage
- notions de TCP et UDP

## Sommaire

- [TP2 : On va router des trucs](#tp2--on-va-router-des-trucs)
  - [Sommaire](#sommaire)
  - [0. Prérequis](#0-prérequis)
  - [I. ARP](#i-arp)
    - [1. Echange ARP](#1-echange-arp)
    - [2. Analyse de trames](#2-analyse-de-trames)
  - [II. Routage](#ii-routage)
    - [1. Mise en place du routage](#1-mise-en-place-du-routage)
    - [2. Analyse de trames](#2-analyse-de-trames-1)
    - [3. Accès internet](#3-accès-internet)
  - [III. DHCP](#iii-dhcp)
    - [1. Mise en place du serveur DHCP](#1-mise-en-place-du-serveur-dhcp)
    - [2. Analyse de trames](#2-analyse-de-trames-2)
  - [IV. TCP et UDP](#iv-tcp-et-udp)
    - [1. `netcat` UDP](#1-netcat-udp)
    - [2. `netcat` TCP](#2-netcat-tcp)
    - [3. Deeper](#3-deeper)
    - [4. Analyse de protocoles communs](#4-analyse-de-protocoles-communs)
  - [Conclusion](#conclusion)

## 0. Prérequis

Pour ce TP, on va se servir de VMs Rocky Linux. 1Go RAM c'est large large. Vous pouvez redescendre la mémoire vidéo aussi.  

Vous aurez besoin de deux réseaux host-only dans VirtualBox :

- un premier réseau `10.2.1.0/24`
- le second `10.2.2.0/24`
- **vous devrez désactiver le DHCP de votre hyperviseur (VirtualBox) et définir les IPs de vos VMs de façon statique**

Quelques paquets seront souvent nécessaires dans les TPs, il peut être bon de les installer dans la VM que vous clonez :

- de quoi avoir les commandes :
  - `dig`
  - `tcpdump`
  - `nmap`
  - `nc`
  - `python3`
  - `vim` peut être une bonne idée

Vous devrez **systématiquement** utiliser SSH pour contrôler vos VMs.

Vos firewalls doivent **toujours** être actifs (et donc correctement configurés).

Aussi, **autre rappel**, vos machines doivent **toujours être nommées** ([définition du hostname](../../cours/memo/rocky_network.md#changer-son-nom-de-domaine)).

Dernière chose : **pour toutes les parties avec Wireshark**, vous devrez systématiquement fournir le fichier `.pcap` dans le rendu de TP. Pour le compte-rendu, vous pouvez screen Wireshark si besoin. Un petit emoji 📁 sera présent à chaque fois qu'une capture réseau (fichier `.pcap`) devra être dans le rendu.

## I. ARP

Première partie simple, on va avoir besoin de 2 VMs.

| Machine          | NAT ? | `10.2.1.0/24` |
|------------------|-------|---------------|
| `node1.net1.tp2` | non   | `10.2.1.11`   |
| `node2.net1.tp2` | non   | `10.2.1.12`   |

```schema
   node1               node2
  ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     │
  └─────┘    └───┘    └─────┘
```

> Référez-vous au [mémo Réseau Rocky](../../cours/memo/rocky_network.md) pour connaître les commandes nécessaire à la réalisation de cette partie.

### 1. Echange ARP

🌞**Générer des requêtes ARP**

- effectuer un `ping` d'une machine à l'autre
- observer les tables ARP des deux machines
- repérer l'adresse MAC de `node1` dans la table ARP de `node2` et vice-versa
- prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
  - une commande pour voir la table ARP de `node1`
  - et une commande pour voir la MAC de `node2`

### 2. Analyse de trames

🌞**Analyse de trames**

- utilisez la commande `tcpdump` pour réaliser une capture de trame
- videz vos tables ARP, sur les deux machines, puis effectuez un `ping`
- stoppez la capture, et exportez-la sur votre hôte pour visualiser avec Wireshark
- mettez en évidence les trames ARP
- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, je veux TOUTES les trames**
  - uniquement l'ARP
  - sans doublons de trame, dans le cas où vous avez des trames en double

Par exemple (copiez-collez ce tableau ce sera le plus simple) :

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | `node1` `AA:BB:CC:DD:EE` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node2` `AA:AA:AA:AA:AA` | `node1` `AA:BB:CC:DD:EE`   |
| ...   | ...         | ...                      | ...                        |

📁 Capture réseau `tp2_arp.pcap`


> Si vous ne savez pas comment récupérer votre fichier `.pcap` sur votre hôte afin de l'ouvrir dans Wireshark, demandez-moi.

## II. Routage

Vous aurez besoin de 3 VMs pour cette partie.

| Machine           | NAT ? | `10.2.1.0/24` | `10.2.2.0/24` |
|-------------------|-------|---------------|---------------|
| `router.net2.tp2` | oui   | `10.2.1.254`   | `10.2.2.254`   |
| `node1.net1.tp2`  | no    | `10.2.1.11`   | no            |
| `marcel.net2.tp2` | no    | no            | `10.2.2.12`   |

> Je l'ai appelé `marcel` histoire de voir un peu plus clair dans les noms 🌻

```schema
   node1               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └───┘    └─────┘    └───┘    └─────┘
```

### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router.net2.tp2`**

> Cette étape est nécessaire car Rocky Linux c'est pas un OS dédié au routage par défaut. Ce n'est bien évidemment une opération qui n'est pas nécessaire sur un équipement routeur dédié comme du matériel Cisco.

🌞**Ajouter les routes statiques nécessaires pour que `node1.net1.tp2` et `marcel.net2.tp2` puissent se `ping`**

- il faut ajouter une seule route des deux côtés
- une fois les routes en place, vérifiez avec un `ping` que les deux machines peuvent se joindre

### 2. Analyse de trames

🌞**Analyse des échanges ARP**

- videz les tables ARP des trois noeuds
- effectuez un `ping` de `node1.net1.tp2` vers `marcel.net2.tp2`
- regardez les tables ARP des trois noeuds
- essayez de déduire un peu les échanges ARP qui ont eu lieu
- répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur `node1` et `marcel`, afin de capturer les échanges depuis les 2 points de vue
- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames** utiles pour l'échange

Par exemple (copiez-collez ce tableau ce sera le plus simple) :

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `node1` `AA:BB:CC:DD:EE`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `marcel` `AA:AA:AA:AA:AA` | x              | `node1` `AA:BB:CC:DD:EE`   |
| ...   | ...         | ...       | ...                       |                |                            |
| ?     | Ping        | ?         | ?                         | ?              | ?                          |
| ?     | Pong        | ?         | ?                         | ?              | ?                          |

📁 Capture réseau `tp2_routage_node1.pcap` `tp2_routage_marcel.pcap`

### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

- le routeur a déjà un accès internet
- ajoutez une route par défaut à `node1.net1.tp2` et `marcel.net2.tp2`
  - vérifiez que vous avez accès internet avec un `ping`
  - le `ping` doit être vers une IP, PAS un nom de domaine
- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser
  - vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`
  - puis avec un `ping` vers un nom de domaine

🌞Analyse de trames

- effectuez un `ping 8.8.8.8` depuis `node1.net1.tp2`
- capturez le ping depuis `node1.net1.tp2` avec `tcpdump`
- analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame | IP source           | MAC source               | IP destination | MAC destination |     |
|-------|------------|---------------------|--------------------------|----------------|-----------------|-----|
| 1     | ping       | `node1` `10.2.1.12` | `node1` `AA:BB:CC:DD:EE` | `8.8.8.8`      | ?               |     |
| 2     | pong       | ...                 | ...                      | ...            | ...             | ... |

📁 Capture réseau `tp2_routage_internet.pcap`

## III. DHCP

On reprend la config précédente, et on ajoutera à la fin de cette partie une 4ème machine pour effectuer des tests.

| Machine           | NAT ? | `10.2.1.0/24`              | `10.2.2.0/24` |
|-------------------|-------|----------------------------|---------------|
| `router.net2.tp2` | oui   | `10.2.1.254`                | `10.2.2.254`   |
| `node1.net1.tp2`  | no    | `10.2.1.11`                | no            |
| `node2.net1.tp2`  | no    | oui mais pas d'IP statique | no            |
| `marcel.net2.tp2` | no    | no                         | `10.2.2.12`   |

```schema
   node1               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └─┬─┘    └─────┘    └───┘    └─────┘
   node2       │
  ┌─────┐      │
  │     │      │
  │     ├──────┘
  └─────┘
```

### 1. Mise en place du serveur DHCP

🌞**Sur la machine `node1.net1.tp2`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `node1.net1.tp2`
- créer une machine `node2.net1.tp2`
- faites lui récupérer une IP en DHCP à l'aide de votre serveur

> Il est possible d'utilise la commande `dhclient` pour forcer à la main, depuis la ligne de commande, la demande d'une IP en DHCP, ou renouveler complètement l'échange DHCP (voir `dhclient -h` puis call me et/ou Google si besoin d'aide).

🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut
  - un serveur DNS à utiliser
- récupérez de nouveau une IP en DHCP sur `node2.net1.tp2` pour tester :
  - `node2.net1.tp2` doit avoir une IP
    - vérifier avec une commande qu'il a récupéré son IP
    - vérifier qu'il peut `ping` sa passerelle
  - il doit avoir une route par défaut
    - vérifier la présence de la route avec une commande
    - vérifier que la route fonctionne avec un `ping` vers une IP
  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne
    - vérifier un `ping` vers un nom de domaine

### 2. Analyse de trames

🌞**Analyse de trames**

- videz les tables ARP des machines concernées
- lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP
  - choisissez vous-mêmes l'interface où lancer la capture
- répéter une opération de renouvellement de bail DHCP, et demander une nouvelle IP afin de générer un échange DHCP
- exportez le fichier `.pcap`
- mettez en évidence l'échange DHCP *DORA* (Disover, Offer, Request, Acknowledge)
- **écrivez, dans l'ordre, les échanges ARP + DHCP qui ont lieu, je veux TOUTES les trames** utiles pour l'échange 

📁 Capture réseau `tp2_dhcp.pcap`

## Conclusion

Dans ce TP, on a exploré :

- le fonctionnement du protocole ARP, nécessaire à la communication au sein d'un LAN
- le fonctionnement du protocole IP, nécessaire pour le routage
  - p'tit big up au DHCP au passage
- le fait que des protocoles communs comme DNS ou HTTP utilise toute cette couche
